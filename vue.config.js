module.exports = {
  css: {
    loaderOptions: {
      scss: {
        data: `@import "@/assets/sass/main.scss";`,
      },
    },
  },
};
